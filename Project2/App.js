/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  Button,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';



//const App: () => React$Node = () => {
export default class App extends Component{

    constructor(props){
    super(props);
    this.state={
        email:"",
        }
    }

    validateField=()=>{
    const{email}=this.state;
    var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if(email.match(mailformat))
        alert("'"+email+ "' is a valid mail  ");
    else
        alert("'"+email+ "' is invalid");


    }

    render(){
  return (
  <View>

        <View style={{height:50,fontWeight:'bolder',backgroundColor:'white'}}>
                <Text style={{fontWeight:'bold',fontSize:30}}> React Native Training </Text>
        </View>

         <View style={{backgroundColor:'gray',height:600}}>
             <TextInput
                    style={{
                      height: 70,
                      height: 70,
                      width:370,
                      left:10,
                      top:150,
                      borderColor: 'black',
                      borderWidth: 3,
                      textAlign:'center',
                      borderRadius:20,
                    }}
                    placeholder="Type Your Email"
                    onChangeText={(value)=>this.setState({email:value})}
                  />

                    <View style={{top:200,left:165,width:60}}>
                            <Button title={'test'}
                             onPress={() => this.validateField()}
                                      />
                            </View>

          </View>

          </View>
  );
  }
}




