import 'react-native-gesture-handler';
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState ,useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  Button,
  FlatList,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
  Image,
} from 'react-native';

import {

  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import {WebView } from 'react-native-webview';
import {Paragraph} from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon1 from  'react-native-vector-icons/Ionicons'
import {Container,Header,Content,Left, Right} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import location from '../Assignment2/location';

/////////////////////////api calls
 





//////////////////////////////////Home Screen
const WeatherScreen=()=>{

      const [isLoading,setLoading]=useState(true);
      const [data,setData]=useState(); 
   showWeather=()=>{
      const req=location.getPosts();
        fetch(req.url)
        .then(setLoading(false))
        .then((responseJson)=>responseJson.json())
        .then(showData)
        .catch((error)=>alert(error))
      
  }

       showData=(data)=>{
         setLoading(true);
         setData(data);
        //  alert(JSON.stringify(data.weather[0].description))
          };

  return(
      
            <ScrollView>
              <Button title="Fetch" onPress={()=>showWeather()} />
              

              {/* {!isLoading &&(
                    <Text style={{color:'green',fontSize:50,left:120,top:40,height:300}}>Loading</Text>    
              )} */}
            

              {isLoading? (
                  <View
                      style={{backgroundColor:'green',height:550
                          ,paddingTop:30,paddingLeft:60,paddingRight:80
                      }}
                    >
                      <Text style={{fontSize:50,left:30,top:200}}>{JSON.stringify(data.weather[0].description)}</Text>
                 </View>
              ):<Text style={{color:'green',fontSize:50,left:120,top:40,height:300}}>Loading</Text> 
                    }


               {/* { data && data.length>0 &&(

                    data.map(obj =>(
                      <View
                      style={{backgroundColor:'green',height:500
                          ,paddingTop:30,paddingLeft:60,paddingRight:80
                      }}
                    >
                      <Text style={{fontSize:20}}>{obj}</Text>
                      </View>

                    ))

                    )} */}

            </ScrollView>
  );
  }



//////////////////////////////API

function GoogleScreen() {
  return (
  
        <WebView
        source={{
          uri: 'https://www.google.com/',
        }}
        style={{ marginTop: 20}}
      />
      
  );
}



const Tab=createBottomTabNavigator();
function HomeScreen({ navigation }) {
  return (
    <NavigationContainer independent={true}>        
      <Header style={{left:-100}}>
          <Left>
          <Icon1 name='md-list' size={50} style={{left:-15}} onPress={()=>navigation.toggleDrawer()}/>
          </Left>
        </Header>
   
      <Tab.Navigator initialRouteName="Weather" >
      <Tab.Screen name="Weather" component={WeatherScreen} options={{
          tabBarLabel: 'Weather',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="weather-cloudy" color={color} size={26} />
          ),
        }} />  
      <Tab.Screen name="Google" component={GoogleScreen}
        options={{
          tabBarLabel: 'Google',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="search-web" color={color} size={26} />
          ),
        }}
      />
      </Tab.Navigator>
    </NavigationContainer>
    

  );
}
// /////////////////////////////////Home Screen






/////////////////////////////Settings screee
function AboutScreen({navigation}){

  return(
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',width:380 ,padding:10,top:-13}}>
    <Paragraph>React Native is an open-source mobile application framework created by Facebook.It is used 
      to develop applications for Android, iOS, macOS, tvOS, Web, Windows and UWP 
      by enabling developers to use React's framework along with native platform capabilities.</Paragraph>
      <Text>{'\n'}</Text>

    <Paragraph>Whenever there is an update for apps written in Swift/Objective-C or Java,
       the whole app needs to be recompiled and a new version has to be distributed to the App Store again.
       All this can take a few weeks depending on the App Store review process.</Paragraph>
       <Text>{'\n'}</Text>
       <Paragraph>To avoid this hassle, React Native apps work in a different way,
          a native app is able to locate specific JavaScript code, which is later downloaded and 
          compiled when the app is launched on an actual device.By this,updating the app 
          can be done instantly without needing to submit a new version to the App Store again and again.</Paragraph>
        
  <Text>{'\n'}</Text>
    <Button title="Go to profile" onPress={()=>navigation.navigate("Profile")}/>
    </View>
  );<Text></Text>
}

function ProfileScreen({route,navigation}){

  const [name,setName]=useState();

  const load=async()=>{

    try {
      
      let name=await AsyncStorage.getItem("Myname");
      if(name!== null)
          setName(name);
      else
          setName("Not set");

    } catch (error) {
      alert(error);
    }
  }

  const remove=async()=>{

    try {

      await AsyncStorage.removeItem("Myname")
      navigation.navigate("About");
      
    } catch (error) {
      alert(error);
      
    }finally{
        setName("");
    }
  }

  useEffect(()=>{

    load();
  },[]);


  return(
  <View>
        <View style={{top:10,width:80,left:320}}> 
        <Button title="Edit" onPress={()=>navigation.navigate("Editp")}/>         
        </View>
 
        <Text 
        style={{left:5,top:-22,
          fontSize:20,width:250,
          height:50,
          color:'green'

        }}
        >Name:{name}</Text>
   


       <View style={{width:85,left:320}}>
        <Button
        title="remove"
        color='red'
        onPress={()=>remove()}
        />
        </View>

    </View>
 
  );
}




function EditProfile({navigation}) {
    const [name,setName]=useState();

    const save=async()=>{

      try {
        await AsyncStorage.setItem("Myname",name);
        navigation.navigate("About")

      } catch (error) {
          alert(error);
      }
    }

    return(
          <View>
            <Image source={require("./assets/welcome.png")}
          style={{width:420,height:200,marginTop:24}}
            resizeMode="contain"/>
            <Text>{"\n"}</Text>
         <Text style={{fontStyle:'italic',fontWeight:'bold',fontSize:20,color:'red'}}>
           Enter your name {"\n"}{"\n"}
         </Text>

         <TextInput 
         style={{borderWidth:2,borderColor:"#575dd9",borderRadius:8,height:60}}
         onChangeText={text=>setName(text)}
         />
         <Text>{"\n"}</Text>

      <View style={{width:100,left:150}}>
        <Button
        title="Save"
        color='green'
        onPress={()=>save()}
        />
        </View>


          </View>

    );

}


const Stack=createStackNavigator();
function SettingsScreen({ props,navigation }) {

//   const [site,setSite]=useState([
//       {name:'About',id:'1'},
//       {name:'Profile',id:'2'}
//   ]);

// const pressHandler=(name)=>{
//     // navigation.navigate(name);
//     alert(name);
// }

  return (

    <NavigationContainer independent={true} >
       <Header style={{left:-100}}>
          <Left>
          <Icon1 name='md-list' size={50} style={{left:-15}} onPress={()=>navigation.toggleDrawer()}/>          
          </Left>
        </Header>

        {/* <View>
          <FlatList
          keyExtractor={(item)=>{item.id}}
          data={site}
          renderItem={(item)=>(
           <TouchableOpacity  onPress={()=>pressHandler(item.name)}>
           <Text style={{fontSize:30,
            marginTop:24,
            padding:30,
            fontSize:24,
            backgroundColor:'cyan',
            marginHorizontal:10
          }} 
          >{item.name}
          </Text>
          </TouchableOpacity>

          )}
          />
        </View> */}
         
         <Stack.Navigator >
         <Stack.Screen name="About" component={AboutScreen} />
          <Stack.Screen name="Profile" component={ProfileScreen}/>
          <Stack.Screen name="Editp" component={EditProfile}  options={{title:"Edit Profile"}}/>   
         </Stack.Navigator>
       </NavigationContainer>
    
 
    
  );
}
//////////////////////////////Settings screen

const Tab1=createBottomTabNavigator();
const Drawer=createDrawerNavigator();
 export default function App() {
  return (
    <NavigationContainer>
    <Drawer.Navigator initialRouteName="Home">
     <Drawer.Screen name="Home"  component={HomeScreen} />
     <Drawer.Screen name="Settings" component={SettingsScreen} />
   </Drawer.Navigator>
   </NavigationContainer>
  );
};








 

